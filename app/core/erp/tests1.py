from builtins import print

from django.test import TestCase

from config.wsgi import *

from core.erp.models import *

# Create your tests here.

# orm de django

#listar
# query = Category.objects.all()
# print(query)

# insertar
# t = Category()
# t.name = 'enlatados'
# t.save()

# editar

# t = Type.objects.get(id=1) #el metodo get recupera el objeto que concuerda con el id=1
# t.names = 'lacteos'
# t.save()


# los errores de excepcion se los puede controlar con un try-catch de la siguiente manera
# try:
#     t = Type.objects.get(id=2)
#     t.names = 'lacteos'
#     t.save()
# except Exception as e:
#     print(e)

# eliminacion

# t = Type.objects.get(id=1)
# t.delete()

#listar con el metodo filter es como trabajar con un filtro como el like en sql

# t = Type.objects.filter(names__contains='la')
# print(t)
# t = Type.objects.filter(names__iendswith='os').exclude(id=2) # excluye el del d = 2
# print(t)
# t = Type.objects.filter(names__contains='la').count()
# print(t)
# t = Type.objects.filter(names__contains='la').query #muestra
# t = Type.objects.filter(names__icontains='la')  # par que no tme en cuenta maysculas y minusculas
# print(t)

# for i in Type.objects.filter(names__icontains='la'):
#     print(i)
#
#
# for i in Type.objects.filter(names__icontains='la'):
#     print(i.names, i.id)


# insertar en el modelo product
# t = Product()
# t.name = 'jugo de frutas'
# t.cate_id = 2
# t.pvp = 2.34
# t.save()
#
# query = Product.objects.all()
# print(query)