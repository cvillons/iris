$(function () {
    // https://datatables.net/manual/ajax
    //     https://datatables.net/examples/ajax/
    //         https://datatables.net/extensions/scroller/examples/initialisation/large_js_source.html

                $('#data').DataTable({
                    responsive: true,
                    autoWidth: false,
                    destroy: true,
                    deferRender: true,
                    ajax: {
                        url: window.location.pathname,
                        type: 'POST',
                        data: {
                            'action': 'searchdata'
                        }, // parametros
                        dataSrc: ""
                    },
                    columns: [
                        {"data": "id"},
                        {"data": "name"},
                        {"data": "desc"},
                        {"data": "desc"},
                    ],
                    columnDefs: [
                        {
                            targets: [-1],
                            class: 'text-center',
                            orderable: false,
                            render: function (data, type, row) {
                                var buttons = '<a href="/erp/category/update/' + row.id + '/" class="btn btn-warning btn-xs "><i class="fas fa-edit"></i></a> ';
                                buttons += '<a href="/erp/category/delete/' + row.id + '/" type="button" class="btn btn-danger btn-xs "><i class="fas fa-trash-alt"></i></a>';
                                return buttons;
                            }
                        },
                    ],
                    initComplete: function (settings, json) {

                    }
                });
});